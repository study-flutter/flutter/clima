import 'package:geolocator/geolocator.dart';

class Location {
  double longitude;
  double latitude;
  Future<void> getCurrentLocation() async {
    try {
      Position positionData =
          await getCurrentPosition(desiredAccuracy: LocationAccuracy.low);
      latitude = positionData.latitude;
      longitude = positionData.longitude;
    } catch (e) {
      print(e);
    }
  }
}
